//
//  AppDelegate.swift
//  CodeSample
//
//  Created by Vista on 25.09.17.
//

import UIKit
import RealmSwift
import Realm

let realm = try! Realm()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}


//
//  EditProfileViewController.swift
//  VistaMed
//
//  Created by Vista on 12.09.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var patrNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var birthDateTextField: UITextField!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var patrNameLabel: UILabel!
    @IBOutlet weak var birthDateLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var editProfileButton: UIButton!
    
    //MARK: - Properties
    var patient     = Patient()
    var haveOffset  = false
    let popoverVCId = "PopoverViewController"
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addGestureRecognizer()
        self.subscription()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeGestureRecognizer()
        self.unsubscription()
    }
    
    //MARK: - IBActions
    @IBAction func editProfileAction(_ sender: UIButton) {

        if Connection.isConnectedToNetwork() {
            
            let email     = self.changesFor(email: emailTextField.text)
            let phone     = self.changesFor(phone: phoneTextField.text)
            let gender    = self.changesFor(gender: genderTextField.text)
            let lastName  = self.changesFor(lastName: lastNameTextField.text)
            let patrName  = self.changesFor(patrName: patrNameTextField.text)
            let firstName = self.changesFor(firstName: firstNameTextField.text)
            let birthDate = self.changesFor(birthDate: birthDateTextField.text)

            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                UserDataManager.sharedManager.editProfile(
                    token: token,
                    lastName: lastName,
                    firstName: firstName,
                    patrName: patrName,
                    birthDate: birthDate,
                    gender: gender,
                    email: email,
                    phone: phone, success: { 
                        
                        self.createAndPresentSimpleAlertWith(message: "Profile successfully edited.".localized())
                        
                }, failure: { (error) in
                    
                    self.createAndPresentSimpleAlertWith(message: "Unable edit profile.".localized())
                    print(error?.localizedDescription ?? "Unable edit profile!!!")
                })
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
        }
    }

    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.birthDateTextField {
            
            self.removeFirstResponder()
            self.presentPopoverWith(state: .date, height: 200, from: textField)
            
            return false
            
        } else if textField == self.genderTextField {
            
            self.removeFirstResponder()
            self.presentPopoverWith(state: .gender, height: 50, from: textField)
            
            return false
        }
        
        return true
    }

    //MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        if let presentedVC = popoverPresentationController.presentedViewController as? PopoverViewController {
            if presentedVC.state == .date {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                let dateString = dateFormatter.string(from: presentedVC.datePicker.date)
                
                self.birthDateTextField.text = dateString
                
            } else if presentedVC.state == .gender {
                
                let index = presentedVC.segmentedControl.selectedSegmentIndex
                
                if index == 0 || index == 1 {
                    
                    let gender = presentedVC.segmentedControl.titleForSegment(at: index)
                    
                    self.genderTextField.text = gender
                }
            }
        }
    }
    
    //MARK: - MyMethods
    fileprivate func settingViews() {
        
        self.getPatientFromRealm()
        
        self.navigationItem.title = "Editing".localized()
        self.view.backgroundColor = .white
        
        self.editProfileButton.setTitle("Edit".localized(), for: .normal)
        
        self.birthDateLabel.text = "Date of birth".localized()
        self.firstNameLabel.text = "First name".localized()
        self.lastNameLabel.text  = "Last name".localized()
        self.patrNameLabel.text  = "Patronymic".localized()
        self.genderLabel.text    = "Gender".localized()
        self.emailLabel.text     = "E-mail"
        self.phoneLabel.text     = "Phone".localized()
        
        self.firstNameTextField.text = self.patient.firstName
        self.lastNameTextField.text  = self.patient.lastName
        self.patrNameTextField.text  = self.patient.patrName
        self.genderTextField.text    = self.patient.gender
        self.emailTextField.text     = self.patient.email
        self.phoneTextField.text     = self.patient.phone
        
        if let birthDate = self.patient.birthDate {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            self.birthDateTextField.text = dateFormatter.string(from: birthDate)
            
        } else {
            self.birthDateTextField.text = "Not specified".localized()
        }
    }
    
    fileprivate func getPatientFromRealm() {
        self.patient = realm.objects(Patient.self).first ?? Patient()
    }
    
    fileprivate func presentPopoverWith(state: PopoverViewController.State, height: CGFloat, from textField: UITextField) {
        
        let popoverVC = self.storyboard?.instantiateViewController(withIdentifier: popoverVCId) as! PopoverViewController
        popoverVC.state = state
        
        popoverVC.modalPresentationStyle = .popover
        popoverVC.preferredContentSize   = CGSize(width: self.view.bounds.width, height: height)
        
        popoverVC.popoverPresentationController?.permittedArrowDirections = .up
        popoverVC.popoverPresentationController?.sourceView               = textField
        popoverVC.popoverPresentationController?.sourceRect               = textField.bounds
        popoverVC.popoverPresentationController?.delegate                 = self
        
        self.present(popoverVC, animated: true, completion: nil)
    }
    
    //For keyboard.
    fileprivate func subscription() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func unsubscription() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect)?.height
        
        if !self.haveOffset {
            self.scrollView.contentSize.height += keyboardHeight ?? 0
            self.haveOffset = !self.haveOffset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect)?.height
        
        if self.haveOffset {
            self.scrollView.contentSize.height -= keyboardHeight ?? 0
            self.haveOffset = !self.haveOffset
        }
    }
    
    //For dismiss keyboard by tap.
    fileprivate func addGestureRecognizer() {
        
        if self.keyboardDismissTapGesture == nil {
            
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }
    
    fileprivate func removeGestureRecognizer() {
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    //For text fields.
    fileprivate func removeFirstResponder() {
        
        if self.lastNameTextField.isFirstResponder {
            self.lastNameTextField.resignFirstResponder()
        } else if self.firstNameTextField.isFirstResponder {
            self.firstNameTextField.resignFirstResponder()
        } else if self.patrNameTextField.isFirstResponder {
            self.patrNameTextField.resignFirstResponder()
        } else if self.emailTextField.isFirstResponder {
            self.emailTextField.resignFirstResponder()
        } else if self.phoneTextField.isFirstResponder {
            self.phoneTextField.resignFirstResponder()
        }
    }
    
    //Alert.
    fileprivate func createAndPresentSimpleAlertWith(message: String) {
        
        let alertVC  = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized(), style: .default, handler: nil)
        
        alertVC.addAction(okAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }

    //MARK: - CheckMethods
    fileprivate func changesFor(lastName: String?) -> String? {
        
        if lastName != nil && lastName != self.patient.lastName {
            return lastName
        } else {
            return nil
        }
    }
    
    fileprivate func changesFor(firstName: String?) -> String? {
        
        if firstName != nil && firstName != self.patient.firstName {
            return firstName
        } else {
            return nil
        }
    }
    
    fileprivate func changesFor(patrName: String?) -> String? {
        
        if patrName != nil && patrName != self.patient.patrName {
            return patrName
        } else {
            return nil
        }
    }
    
    fileprivate func changesFor(email: String?) -> String? {
        
        if email != nil && email != self.patient.email {
            return email
        } else {
            return nil
        }
    }
    
    fileprivate func changesFor(phone: String?) -> String? {
        
        if phone != nil && phone != self.patient.phone {
            return phone
        } else {
            return nil
        }
    }
    
    
    fileprivate func changesFor(birthDate: String?) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        if birthDate != nil, let date = dateFormatter.date(from: birthDate!), date != self.patient.birthDate {
            return date
        } else {
            return nil
        }
    }
    
    fileprivate func changesFor(gender: String?) -> String? {
        
        if gender != nil && gender != self.patient.gender {
            
            if gender == "Male".localized() {
                return "MALE"
            } else if gender == "Female".localized() {
                return "FEMALE"
            } else {
                return nil
            }
            
        } else {
            return nil
        }
    }
}

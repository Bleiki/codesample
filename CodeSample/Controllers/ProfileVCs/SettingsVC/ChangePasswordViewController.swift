//
//  ChangePasswordViewController.swift
//  VistaProject
//
//  Created by Admin on 06.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var repeatNewPasswordTextField: UITextField!
    
    //MARK: - Properties
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addGestureRecognizer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeGestureRecognizer()
    }

    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.newPasswordTextField {
            self.repeatNewPasswordTextField.becomeFirstResponder()
        } else {
            self.repeatNewPasswordTextField.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: - IBActions
    @IBAction private func changePasswordAction(_ sender: UIButton) {
        
        self.removeFirstResponder()
        
        let alertVC = UIAlertController(title: nil, message: "Are you sure that you want change password?".localized(), preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
            self.changePassword()
        }
        
        let noAction = UIAlertAction(title: "No".localized(), style: .default) { (action) in
        }
        
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    //MARK: - MyMethods
    fileprivate func addGestureRecognizer() {
        
        if self.keyboardDismissTapGesture == nil {
            
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }
    
    fileprivate func removeGestureRecognizer() {
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    fileprivate func settingViews() {
        
        self.navigationItem.title = "Settings".localized()
        
        self.newPasswordTextField.placeholder       = "Enter password...".localized()
        self.repeatNewPasswordTextField.placeholder = "Repeat password...".localized()
        
        self.changePasswordButton.setTitle("Change password".localized(), for: .normal)
    }
    
    fileprivate func removeFirstResponder() {
        
        if self.repeatNewPasswordTextField.isFirstResponder {
            
            self.repeatNewPasswordTextField.resignFirstResponder()
            
        } else if self.newPasswordTextField.isFirstResponder {
            
            self.newPasswordTextField.resignFirstResponder()
        }
    }
    
    fileprivate func clearTextFields() {
        self.newPasswordTextField.text       = ""
        self.repeatNewPasswordTextField.text = ""
    }
    
    func changePassword() {
        
        if let newPas = self.newPasswordTextField.text, let repeatNewPas = self.repeatNewPasswordTextField.text {
            if newPas.characters.count != 0, repeatNewPas.characters.count != 0 {
                if newPas == repeatNewPas {
                    if Connection.isConnectedToNetwork() {
                        if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                            
                            UserDataManager.sharedManager.changePassword(token: token, newPassword: newPas, success: { () in
                                
                                self.createAndPresentSimpleAlertWith(message: "Password update successfully.".localized())
                                self.clearTextFields()
                                
                            }, failure: { (error) in
                                
                                self.createAndPresentSimpleAlertWith(message: "Unable to update password.".localized())
                                print(error ?? "Can't change password!!!")
                                
                            })
                            
                        } else {
                            self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                        }
                    } else {
                        self.createAndPresentSimpleAlertWith(message: "No internet conection.".localized())
                    }
                } else {
                    self.createAndPresentSimpleAlertWith(message: "Passwords must match.".localized())
                }
            } else {
                self.createAndPresentSimpleAlertWith(message: "Fill in the fields.".localized())
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "Fil in the fields.".localized())
        }
    }
}

//
//  ProfileViewController.swift
//  VistaProject
//
//  Created by Admin on 13.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    let changePasswordVCId = "ChangePasswordViewController"
    let autorizationVCId   = "AutorizationViewController"
    let editProfileVCId    = "EditProfileViewController"
    let documentsVCId      = "DocumentTypesViewController"
    
    let accessTokenID = 1
    
    var patient     = Patient()
    var patientInfo = [String]()
    let parameters  = ["Last name".localized(), "First name".localized(), "Patronymic".localized(), "Date of birth".localized(), "Gender".localized(), "Phone".localized(), "E-mail", "Documents".localized()]
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.settingView()
        self.getPatientInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.checkAutorization()
    }

    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.patientInfo.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ProfileCell"
        let parameter  = patientInfo[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text = parameter
        cell.accessoryType   = .none
        
        switch parameter {
        case "Last name".localized():
            cell.detailTextLabel?.text = self.patient.lastName
        case "First name".localized():
            cell.detailTextLabel?.text = self.patient.firstName
        case "Patronymic".localized():
            cell.detailTextLabel?.text = self.patient.patrName
        case "Gender".localized():
            cell.detailTextLabel?.text = self.patient.gender
        case "Date of birth".localized():
            
            if self.patient.birthDate != nil {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                cell.detailTextLabel?.text = dateFormatter.string(from: self.patient.birthDate!)
                
            } else {
                cell.detailTextLabel?.text = "Not specified".localized()
            }

        case "Phone".localized():
            cell.detailTextLabel?.text = self.patient.phone
        case "E-mail":
            cell.detailTextLabel?.text = self.patient.email
        case "Documents".localized():
            cell.detailTextLabel?.text = ""
            cell.accessoryType         = .disclosureIndicator
        default:
            break
        }
        
        return cell
    }
    
    //MARK: - TableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == self.patientInfo.count - 1 {
            let documentTypesVC = self.storyboard?.instantiateViewController(withIdentifier: documentsVCId) as! DocumentTypesViewController
            self.navigationController?.pushViewController(documentTypesVC, animated: true)
        }
    }
    
    //MARK: - MyMethods
    func getPatientInfo() {
        
        self.createAndRunActivityIndicatorView()
        
        self.getPatientInfoFromServer(firstAppearance: true) {
            self.stopActivityIndicatorView()
        }
    }
    
    @objc func refreshData() {
        
        if #available(iOS 10.0, *) {

            self.getPatientInfoFromServer(firstAppearance: false) {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    fileprivate func getPatientInfoFromServer(firstAppearance: Bool, _ stopActInd: @escaping () -> ()) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                UserDataManager.sharedManager.getPatientInfoWith(token: token, success: { (patient) in
                    
                    self.patient     = patient
                    self.patientInfo = self.parameters
                    
                    stopActInd()
                    
                    self.firstAppearance = firstAppearance
                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                }, failure: { (error) in
                    
                    stopActInd()
                    
                    self.getPatientInfoFromRealm()
                    print(error ?? "Can't get patient info!!!")
                    
                })
                
            } else {
                
                stopActInd()
                self.getPatientInfoFromRealm()
                print("Haven't access token!!!")
            }
            
        } else {
            
            stopActInd()
            self.getPatientInfoFromRealm()
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func getPatientInfoFromRealm() {

        self.patient     = realm.objects(Patient.self).first ?? Patient()
        self.patientInfo = self.parameters
        
        self.firstAppearance = true
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
    }
    
    //For present autorizationVC if needed.
    fileprivate func checkAutorization() {
        
        if realm.object(ofType: AccessToken.self, forPrimaryKey: self.accessTokenID) == nil {
            
            let autorizationVC = self.storyboard?.instantiateViewController(withIdentifier: self.autorizationVCId) as! AutorizationViewController
            autorizationVC.previousVC = self
            
            self.present(autorizationVC, animated: true, completion: nil)
        }
    }
    
    fileprivate func createMenuBarBtn() {
        
        let image   = UIImage(named: "menu")
        let menuBtn = UIBarButtonItem(image: image, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        self.navigationItem.setLeftBarButton(menuBtn, animated: true)
    }
    
    fileprivate func settingView() {
        
        self.createMenuBarBtn()
        self.addRefreshControl()
        self.createSettingsBarBtn()
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Profile".localized()
        self.firstAppearance           = true
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
    
    //MARK: - SettingsMethods
    fileprivate func createSettingsBarBtn() {
        let settingBtn = UIBarButtonItem(image: UIImage(named: "settings"), style: .done, target: self, action: #selector(settings))
        self.navigationItem.setRightBarButton(settingBtn, animated: true)
    }
    
    @objc func settings() {
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction    = UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil)
        
        alertController.addAction(self.getChangePasswordAction())
        alertController.addAction(self.getClearCacheAction())
        alertController.addAction(self.getEditProfileAction())
        alertController.addAction(self.getLogOutAction())
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func getChangePasswordAction() -> UIAlertAction {
        
        let action = UIAlertAction(title: "Change password".localized(), style: .default) { (action) in

            //Go to change password view controller.
            let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: self.changePasswordVCId) as! ChangePasswordViewController
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
        
        return action
    }
    
    fileprivate func getEditProfileAction() -> UIAlertAction {
        
        let action = UIAlertAction(title: "Edit profile".localized(), style: .default) { (action) in
            
            let editProfileVC = self.storyboard?.instantiateViewController(withIdentifier: self.editProfileVCId) as! EditProfileViewController
            self.navigationController?.pushViewController(editProfileVC, animated: true)
        }
        
        return action
    }
    
    fileprivate func getLogOutAction() -> UIAlertAction {
        
        let action = UIAlertAction(title: "Log out".localized(), style: .default) { (action) in
            
            let alertVC = UIAlertController(title: nil, message: "Are you sure that you want log out?".localized(), preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
                
                let autorizationVC = self.storyboard?.instantiateViewController(withIdentifier: self.autorizationVCId) as! AutorizationViewController
                autorizationVC.previousVC = self
                
                self.present(autorizationVC, animated: true, completion: {
                    RealmDataManager.sharedManager.deleteAllObjects()
                })
            }
            
            let noAction = UIAlertAction(title: "No".localized(), style: .default, handler: nil)
            
            alertVC.addAction(yesAction)
            alertVC.addAction(noAction)
            
            self.present(alertVC, animated: true, completion: nil)
        }
        
        return action
    }
    
    fileprivate func getClearCacheAction() -> UIAlertAction {
        
        let action = UIAlertAction(title: "Clear cache".localized() + " (\(self.getCashSize()))", style: .default) { (action) in
            
            let alert = UIAlertController(title: nil, message: "Are you sure that you want clear cache?".localized(), preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Yes".localized(), style: .default, handler: { (action) in
                self.clearCache()
            })
            
            let noAction = UIAlertAction(title: "No".localized(), style: .default, handler: nil)
            
            alert.addAction(noAction)
            alert.addAction(yesAction)
            
            self.present(alert, animated: true, completion: nil)
        }
        
        return action
    }
    
    fileprivate func getCashSize() -> String {
        
        var multiplicity = 0
        var cashSize     = 0
        var sizes        = ["byte".localized(), "Kb".localized(), "Mb".localized(), "Gb".localized()]
        
        let savedResults   = realm.objects(Result.self).filter("pathToHtmlFile != nil")
        let savedFragments = realm.objects(Fragment.self).filter("pathToHtmlFile != nil")
        
        for result in savedResults {
            cashSize += result.size
        }
        
        for fragment in savedFragments {
            cashSize += fragment.size
        }
        
        while cashSize / 1024 >= 1 {
            cashSize = cashSize / 1024
            multiplicity += 1
        }
        
        var result = "\(cashSize)"
        
        if multiplicity < sizes.count {
            result += " " + sizes[multiplicity]
        }
        
        return result
    }
    
    fileprivate func clearCache() {
        
        //Delete saved html files.
        let savedResults   = realm.objects(Result.self).filter("pathToHtmlFile != nil")
        let savedFragments = realm.objects(Fragment.self).filter("pathToHtmlFile != nil")
        
        for result in savedResults {
            
            do {
                let fileManager = FileManager.default
                
                if fileManager.fileExists(atPath: result.pathToHtmlFile!) {
                    
                    try fileManager.removeItem(atPath: result.pathToHtmlFile!)
                    
                    try realm.write {
                        result.pathToHtmlFile = nil
                    }
                    
                } else {
                    print("Result don't exist!!!")
                }
            } catch {
                print("Can't delete result HTML file!!!")
            }
        }
        
        for fragment in savedFragments {
            
            do {
                let fileManager = FileManager.default
                
                if fileManager.fileExists(atPath: fragment.pathToHtmlFile!) {
                    
                    try fileManager.removeItem(atPath: fragment.pathToHtmlFile!)
                    
                    try realm.write {
                        fragment.pathToHtmlFile = nil
                    }
                    
                } else {
                    print("Fragment don't exist!!!")
                }
            } catch {
                print("Can't delete fragment HTML file!!!")
            }
        }
    }
    
    
}

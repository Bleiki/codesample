//
//  DocumentInfoViewController.swift
//  VistaProject
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class DocumentInfoViewController: BaseViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var seriaTextField: UITextField!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var issuerTextField: UITextField!
    @IBOutlet weak var issueDateTextField: UITextField!
    
    @IBOutlet weak var issueDateLabel: UILabel!
    @IBOutlet weak var issuerLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var seriaLabel: UILabel!
    
    //MARK: - Properties
    var docType: DocumentType?
    var document: Document?
    var isCreating: Bool!
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    let popoverVCId = "PopoverViewController"
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setColorSchemeAndTitles()
        self.selectionOfFollowUpActions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addGestureRecognizer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeGestureRecognizer()
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.seriaTextField {
            
            self.numberTextField.becomeFirstResponder()
            
        } else if textField == self.numberTextField {
            
            self.issuerTextField.becomeFirstResponder()
            
        } else if textField == self.issuerTextField {
            
            textField.resignFirstResponder()
            self.issueDateTextField.becomeFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.issueDateTextField {
            
            self.removeFirstResponder()
            
            let popoverVC = self.storyboard?.instantiateViewController(withIdentifier: popoverVCId) as! PopoverViewController
            popoverVC.state = .date
            
            popoverVC.modalPresentationStyle = .popover
            popoverVC.preferredContentSize   = CGSize(width: self.view.bounds.width, height: 200)
                        
            popoverVC.popoverPresentationController?.permittedArrowDirections = .up
            popoverVC.popoverPresentationController?.sourceView               = textField
            popoverVC.popoverPresentationController?.sourceRect               = textField.bounds
            popoverVC.popoverPresentationController?.delegate                 = self

            self.present(popoverVC, animated: true, completion: nil)
            
            return false
            
        } else {
            
            return true
        }
    }
    
    //MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {

        if let presentedVC = popoverPresentationController.presentedViewController as? PopoverViewController {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            let dateString = dateFormatter.string(from: presentedVC.datePicker.date)
            
            self.issueDateTextField.text = dateString
        }
    }

    //MARK: - MyMethods
    fileprivate func selectionOfFollowUpActions() {
        
        if self.isCreating {
            
            self.settingViews()
            self.navigationItem.title = "Addition".localized()
            
        } else {
            
            self.setDocInfo()
            self.navigationItem.title = "Information".localized()
        }
    }
    
    fileprivate func settingViews() {
        
        self.createSaveBarBtn()
        
        self.seriaTextField.isEnabled     = true
        self.numberTextField.isEnabled    = true
        self.issuerTextField.isEnabled    = true
        self.issueDateTextField.isEnabled = true
    }
    
    fileprivate func setDocInfo() {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        self.seriaTextField.text     = self.document?.serial
        self.numberTextField.text    = self.document?.number
        self.issuerTextField.text    = self.document?.issuedBy
        self.issueDateTextField.text = dateFormatter.string(from: self.document?.issueDate ?? Date())
    }
    
    //For dismiss keyboard by tap.
    fileprivate func addGestureRecognizer() {
        
        if self.keyboardDismissTapGesture == nil {
            
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }
    
    fileprivate func removeGestureRecognizer() {
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    fileprivate func createSaveBarBtn() {
        let saveBtn = UIBarButtonItem(title: "Add".localized(), style: .plain, target: self, action: #selector(saveDocument))
        self.navigationItem.setRightBarButton(saveBtn, animated: true)
    }
    
    @objc func saveDocument() {
        
        self.removeFirstResponder()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        if Connection.isConnectedToNetwork() {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy"
            
            let serial         = self.seriaTextField.text
            let number         = self.numberTextField.text
            let issuedBy       = self.issuerTextField.text
            let issueDateText  = self.issueDateTextField.text
            
            let haveText = self.haveText(serial: serial, number: number, issuedBy: issuedBy, issueDate: issueDateText)
            
            if haveText {
                
                let issueDate = dateFormatter.date(from: issueDateText!)
                
                    if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                        
                        DocumentsDataManager.sharedManager.createDocumentWith(
                            token: token,
                            type: self.docType!.desc,
                            serial: serial!,
                            number: number!,
                            issuedBy: issuedBy!,
                            issueDate: issueDate!, success: {
                                
                            self.createAndPresentSimpleAlertWith(message: "Document successfully added.".localized())
                            self.clearTextFields()
                            self.navigationItem.rightBarButtonItem?.isEnabled = true
                                
                        }, failure: { (error) in
                            
                            self.createAndPresentSimpleAlertWith(message: "Unable to add document.".localized())
                            self.navigationItem.rightBarButtonItem?.isEnabled = true
                            print(error ?? "Can't create document!!!")
                            
                        })

                    } else {
                        self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                        self.navigationItem.rightBarButtonItem?.isEnabled = true
                    }
                } else {
                    self.createAndPresentSimpleAlertWith(message: "Data entered incorrectly.".localized())
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    fileprivate func haveText(serial: String?, number: String?, issuedBy: String?, issueDate: String?) -> Bool {
        
        if serial != nil, number != nil, issuedBy != nil, issueDate != nil, self.docType != nil {
            
            let checkSerial    = serial!.replacingOccurrences(of: " ", with: "")
            let checkNumber    = number!.replacingOccurrences(of: " ", with: "")
            let checkIssuer    = issuedBy!.replacingOccurrences(of: " ", with: "")
            let checkIssueDate = issueDate!.replacingOccurrences(of: " ", with: "")
            
            if checkSerial.characters.count >= 1, checkNumber.characters.count >= 1, checkIssuer.characters.count >= 1, checkIssueDate.characters.count >= 1 {
                
                return true
                
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    fileprivate func removeFirstResponder() {
        
        if self.seriaTextField.isFirstResponder {
            
            self.seriaTextField.resignFirstResponder()
            
        } else if self.numberTextField.isFirstResponder {
            
            self.numberTextField.resignFirstResponder()
            
        } else if self.issuerTextField.isFirstResponder {
            
            self.issuerTextField.resignFirstResponder()
        }
    }
    
    fileprivate func clearTextFields() {
        
        self.seriaTextField.text     = ""
        self.numberTextField.text    = ""
        self.issuerTextField.text    = ""
        self.issueDateTextField.text = ""
    }
    
    fileprivate func setColorSchemeAndTitles() {
        
        if isCreating {
            
            self.seriaTextField.placeholder     = "Enter series...".localized()
            self.numberTextField.placeholder    = "Enter number...".localized()
            self.issuerTextField.placeholder    = "Enter the departament...".localized()
            self.issueDateTextField.placeholder = "Enter the date of issue...".localized()
        }
        
        self.seriaLabel.text     = "Series".localized()
        self.numberLabel.text    = "Number".localized()
        self.issuerLabel.text    = "Issued by".localized()
        self.issueDateLabel.text = "Date of issue".localized()
    }
}

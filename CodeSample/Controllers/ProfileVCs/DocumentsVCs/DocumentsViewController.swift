//
//  DocumentsViewController.swift
//  VistaProject
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class DocumentsViewController: BaseViewController, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDocumentsLabel: UILabel!
    
    //MARK: - Properties
    let docInfoVCId = "DocumentInfoViewController"
    var documents   = [Document]()
    var docType: DocumentType!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingView()
        self.createAddBarBtn()
        self.addRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getDocumentsFromRealm()
    }

    //MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.documents.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let identifier = "DocumentsCell"
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text = self.docType.name + " " + "\(indexPath.row + 1)"
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let document = self.documents[indexPath.row]
        
        tableView.setEditing(false, animated: true)
        
        if !document.fromEsia {
            
            if editingStyle == .delete, let token = UserDataManager.sharedManager.currentAccessToken?.token  {
                
                let message = "Are you sure that you want to delete this document?".localized()
                let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                
                let yesAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
                    
                    DocumentsDataManager.sharedManager.deleteDocumentWith(token: token, id: document.id, success: {
                        
                        self.documents.remove(at: indexPath.row)
                        
                        do {
                            try realm.write {
                                realm.delete(document)
                            }
                        } catch {
                            print("Can't delete document from Realm!!!")
                        }
                        
                        tableView.deleteRows(at: [indexPath], with: .left)
                        
                        self.checkDocuments()
                        
                        self.createAndPresentSimpleAlertWith(message: "Document successfully deleted.".localized())
                        
                    }, failure: { (error) in
                        
                        self.createAndPresentSimpleAlertWith(message: "Unable to delete document.".localized())
                        print(error ?? "Can't delete document!!!")
                        
                    })
                }
                
                let noAction = UIAlertAction(title: "No".localized(), style: .default, handler: nil)
                
                alertVC.addAction(noAction)
                alertVC.addAction(yesAction)
                
                self.present(alertVC, animated: true, completion: nil)
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Unable to delete this document.".localized())
            }
            
        } else {
            self.createAndPresentSimpleAlertWith(message: "You can't delete this document.".localized())
        }
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let docInfoVC = self.storyboard?.instantiateViewController(withIdentifier: self.docInfoVCId) as! DocumentInfoViewController

        docInfoVC.document   = self.documents[indexPath.row]
        docInfoVC.isCreating = false
        
        self.navigationController?.pushViewController(docInfoVC, animated: true)
    }
    
    //MARK: - MyMethods
    func getDocumentsFromRealm() {
        
        let predicate  = NSPredicate(format: "type == %@", argumentArray: [self.docType.desc])
        self.documents = Array(realm.objects(Document.self).filter(predicate))
        
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
        
        self.checkDocuments()
    }

    @objc func refreshData() {

        if #available(iOS 10.0, *) {
            
            let predicate  = NSPredicate(format: "type == %@", argumentArray: [self.docType.desc])
            self.documents = Array(realm.objects(Document.self).filter(predicate))
            
            self.tableView.reloadData()
            
            self.tableView.refreshControl?.endRefreshing()
            
            self.checkDocuments()
        }
    }
    
    fileprivate func createAddBarBtn() {
        let addBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addDocument))
        self.navigationItem.setRightBarButton(addBtn, animated: true)
    }
    
    @objc func addDocument() {
        
        let docInfoVC = self.storyboard?.instantiateViewController(withIdentifier: self.docInfoVCId) as! DocumentInfoViewController
        
        docInfoVC.isCreating = true
        docInfoVC.docType    = self.docType
        
        self.navigationController?.pushViewController(docInfoVC, animated: true)
    }
    
    fileprivate func checkDocuments() {
        
        if self.documents.isEmpty {
            
            self.noDocumentsLabel.text     = "No added documents.".localized()
            self.noDocumentsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noDocumentsLabel)
            
        } else {
            
            self.noDocumentsLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = self.docType.name
        self.firstAppearance           = true
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
}

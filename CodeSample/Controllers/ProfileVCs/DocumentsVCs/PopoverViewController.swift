//
//  PopoverViewController.swift
//  VistaProject
//
//  Created by Admin on 03.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class PopoverViewController: UIViewController {
    
    enum State {
        case date
        case gender
    }

    //MARK: - IBOutlets
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //MARK: - Properties
    var state: State?
        
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white 
        self.selectView()
    }
    
    //MARK: - MyMethods
    fileprivate func settingSegmentedControl() {
        
        self.segmentedControl.setTitle("Male".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Female".localized(), forSegmentAt: 1)
        
        self.segmentedControl.backgroundColor = .clear
        self.segmentedControl.tintColor       = .black
        
        self.segmentedControl.selectedSegmentIndex = -1
    }
    
    fileprivate func selectView() {
        
        if state == .gender {
            
            self.segmentedControl.isHidden = false
            self.settingSegmentedControl()
            
        } else if state == .date {
            
            self.datePicker.isHidden = false
        }
    }
}

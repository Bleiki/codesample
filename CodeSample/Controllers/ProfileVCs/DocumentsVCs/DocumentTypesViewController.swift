//
//  DocumentTypesViewController.swift
//  VistaProject
//
//  Created by Admin on 27.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class DocumentTypesViewController: BaseViewController, UITableViewDataSource {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    let documentsVCId = "DocumentsViewController"
    var docTypes      = [DocumentType]()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingView()
        self.getDocumentTypes()
        self.addRefreshControl()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docTypes.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "DocumentTypesCell"
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text = self.docTypes[indexPath.row].name
        
        return cell
    }
    
    //MARK: - TableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
                
        let documentsVC = self.storyboard?.instantiateViewController(withIdentifier: self.documentsVCId) as! DocumentsViewController
        documentsVC.docType = self.docTypes[indexPath.row]
        
        self.navigationController?.pushViewController(documentsVC, animated: true)
    }
    
    //MARK: - MyMethods
    func getDocumentTypes() {

        self.createAndRunActivityIndicatorView()

        self.getDocumentTypesFromServer(firstAppearance: true) {
            self.stopActivityIndicatorView()
        }
    }
    
    @objc func refreshData() {
        
        if #available(iOS 10.0, *) {

            self.getDocumentTypesFromServer(firstAppearance: false) {
                
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    fileprivate func getDocumentTypesFromServer(firstAppearance: Bool, _ stopActInd: @escaping () -> ()) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                DocumentsDataManager.sharedManager.getDocumentTypes(token: token, success: { (documentTypes) in
                    
                    self.docTypes = documentTypes
                    
                    stopActInd()
                    
                    self.firstAppearance = firstAppearance
                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                }, failure: { (error) in
                    
                    stopActInd()
                    
                    self.getDocumentTypesFromRealm()
                    print(error ?? "Can't get document types!!!")
                    
                })
                
                DocumentsDataManager.sharedManager.getDocuments(token: token)
                
            } else {
                
                stopActInd()
                self.getDocumentTypesFromRealm()
                print("Haven't access token!!!")
            }
            
        } else {
            
            stopActInd()
            self.getDocumentTypesFromRealm()
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func getDocumentTypesFromRealm() {
        
        self.docTypes = Array(realm.objects(DocumentType.self))
        
        self.firstAppearance = true
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Documents".localized()
        self.firstAppearance           = true
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
}

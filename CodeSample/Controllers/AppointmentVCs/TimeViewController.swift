//
//  TimeViewController.swift
//  VistaProject
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class TimeViewController: BaseViewController, UITableViewDataSource {

    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noTalonsLabel: UILabel!
    
    //MARK: - ConstantsAndVariables
    var talons = [Talon]()
    
    var date: Date!
    var doctor: Doctor!
    var clinic: Clinic!

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getTalons()
        self.settingView()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.talons.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "TimeCell"
        let ticket     = self.talons[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text = timeFormatter.string(from: ticket.start)
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.createAndPresentAlertForMakeAnAppointmentAt(talon: self.talons[indexPath.row])
    }

    //MARK: - MyMethods
    fileprivate func getTalons() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()

                AppointmentsDataManager.sharedManager.getTalonListFor(doctor: self.doctor, date: self.date, token: token, success: { (talons) in
                    
                    self.talons = talons.sorted(by: { $0.start < $1.start })
                    
                    self.stopActivityIndicatorView()

                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)

                    self.checkTalons()
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()

                    self.createAndPresentSimpleAlertWith(message: "Could not get a list of talons.".localized())
                    print(error ?? "Can't get time!!!")
                    
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                print("Haven't access token!!!")
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func createAndPresentAlertForMakeAnAppointmentAt(talon: Talon) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "HH:mm"
        
        let title   = "Appointment".localized()
        let message = "To make an appointment with the doctor".localized() + " \(self.doctor.name) " + "on".localized() + " \(dateFormatter.string(from: self.date)) " + "at".localized() + " \(timeFormatter.string(from: talon.start))?"
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
            self.makeAnAppointmentAt(talon: talon)
        }
        
        let noAction = UIAlertAction(title: "No".localized(), style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertVC.addAction(noAction)
        alertVC.addAction(yesAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    fileprivate func makeAnAppointmentAt(talon: Talon) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()
                
                AppointmentsDataManager.sharedManager.makeAnAppointmentTo(doctor: self.doctor, id: talon.id, token: token, success: { () in
                    
                    self.stopActivityIndicatorView()
                    self.saveSupportObjects()
                    self.createAndPresentSimpleAlertWith(message: "You have successfully signed up for an appointment with a doctor.".localized())
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()
                    self.createAndPresentSimpleAlertWith(message: "Exceeded waiting time. If the appointment is not created, contact the technical support.".localized()) //Change if appointment by netrika will correct work.
                    self.saveSupportObjects() 
                    
                    print(error ?? "Can't make an appointment!!!")
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems".localized())
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
        }
    }
    
    fileprivate func saveSupportObjects() {
        
        let clinicPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@", argumentArray: [self.clinic.hostId, self.clinic.clinicId])
        let doctorPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && doctorId == %@", argumentArray: [self.doctor.hostId, self.doctor.clinicId, self.doctor.doctorId])
        
        if realm.objects(Clinic.self).filter(clinicPredicate).first == nil {
            RealmDataManager.sharedManager.saveObject(object: self.clinic)
        }
        
        if realm.objects(Doctor.self).filter(doctorPredicate).first == nil {
            RealmDataManager.sharedManager.saveObject(object: self.doctor)
        }
    }
    
    fileprivate func checkTalons() {
        
        if self.talons.isEmpty {
            
            self.noTalonsLabel.text     = "There are no talons available.".localized()
            self.noTalonsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noTalonsLabel)
            
        } else {
            self.noTalonsLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Talons".localized()
        self.firstAppearance           = true
    }
}

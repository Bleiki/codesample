//
//  SpecialitiesViewController.swift
//  VistaProject
//
//  Created by Admin on 10.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class SpecialitiesViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noSpecialitiesLabel: UILabel!
    
    //MARK: - Properties
    var clinic: Clinic!
    let serachVCId   = "SearchViewController"
    let doctorsVCId  = "DoctorsViewController"
    var specialities = [Speciality]()
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingView()
        self.getSpecialities()
        self.createSearchBarBtn()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.specialities.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "SpecialitiesCell"
        let speciality = self.specialities[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text       = speciality.name
        cell.detailTextLabel?.text = "Free talons".localized() + ": " + "\(speciality.ticketCount)"
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let doctorsVC = self.storyboard?.instantiateViewController(withIdentifier: self.doctorsVCId) as! DoctorsViewController
        
        doctorsVC.speciality = self.specialities[indexPath.row]
        doctorsVC.clinic     = self.clinic
        
        self.navigationController?.pushViewController(doctorsVC, animated: true)
    }
    
    //MARK: - MyMethods
    fileprivate func getSpecialities() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()
                
                AppointmentsDataManager.sharedManager.getSpecialitiesFromClinic(clinic: self.clinic, token: token, success: { (specialities) in
                    
                    self.specialities = specialities.sorted(by: { $0.name < $1.name })
                    
                    self.stopActivityIndicatorView()

                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                    self.checkSpecialities()
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()

                    self.createAndPresentSimpleAlertWith(message: "Could not get a list of specialities.".localized())
                    print(error ?? "Can't get specialities!!!")
                    
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                print("Haven't access token!!!")
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func createSearchBarBtn() {
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(goToSearchVC))
        
        self.navigationItem.setRightBarButton(searchBtn, animated: true)
    }
    
    @objc func goToSearchVC() {
        
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: self.serachVCId) as! SearchViewController
        searchVC.previousVC = self
        
        self.present(searchVC, animated: true, completion: nil)
    }
    
    fileprivate func checkSpecialities() {
        
        if self.specialities.isEmpty {
            
            self.noSpecialitiesLabel.text     = "There are no specialities available.".localized()
            self.noSpecialitiesLabel.isHidden = false
            self.view.bringSubview(toFront: self.noSpecialitiesLabel)
            
        } else {
            self.noSpecialitiesLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Specialities".localized()
        self.firstAppearance           = true
    }
}

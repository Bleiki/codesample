//
//  DatesViewController.swift
//  VistaProject
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class DatesViewController: BaseViewController, UITableViewDataSource {

    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDatesLabel: UILabel!
    
    //MARK: - Properties
    var dates     = [Date]()
    let timesVCId = "TimeViewController"
    
    var doctor: Doctor!
    var clinic: Clinic!

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getDates()
        self.settingView()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dates.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "DatesCell"
        let date       = self.dates[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text = dateFormatter.string(from: date)
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let timeVC = self.storyboard?.instantiateViewController(withIdentifier: self.timesVCId) as! TimeViewController
        timeVC.date   = self.dates[indexPath.row]
        timeVC.doctor = self.doctor
        timeVC.clinic = self.clinic
        
        self.navigationController?.pushViewController(timeVC, animated: true)
    }
    
    //MARK: - MyMethods
    fileprivate func getDates() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()

                AppointmentsDataManager.sharedManager.getDatesListFor(doctor: self.doctor, token: token, success: { (dates) in
                    
                    self.dates = dates
                    
                    self.stopActivityIndicatorView()

                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)

                    self.checkDates()
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()

                    self.createAndPresentSimpleAlertWith(message: "Could not get a list of dates.".localized())
                    print(error ?? "Can't get dates!!!")
                    
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                print("Haven't access token!!!")
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func checkDates() {
        
        if self.dates.isEmpty {
            
            self.noDatesLabel.text     = "There are no dates available.".localized()
            self.noDatesLabel.isHidden = false
            self.view.bringSubview(toFront: self.noDatesLabel)
            
        } else {
            self.noDatesLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Dates".localized()
        self.firstAppearance           = true
    }
}

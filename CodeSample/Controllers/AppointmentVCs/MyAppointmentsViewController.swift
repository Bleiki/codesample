//
//  MyAppointmentsViewController.swift
//  VistaProject
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit
import UserNotifications

class MyAppointmentsViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noAppointmentsLabel: UILabel!
    
    //MARK: - Properties
    var appointments = [Appointment]()
    let clinicsVCId  = "ClinicsViewController"
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For automatic dimension height for table view row.
        self.tableView.rowHeight          = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        
        self.settingController()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.appointments.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let appointment = self.appointments[indexPath.row]
        let identifier  = "MyAppointmentsCell"
        let cell        = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let clinicPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@", argumentArray: [appointment.hostId, appointment.clinicId])
        let doctorPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && doctorId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.doctorId])
        
        let specialityPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && specialityId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.specialityId])
        
        let clinicName     = realm.objects(Clinic.self).filter(clinicPredicate).first?.name ?? "The polyclinic is determining".localized()
        let doctorName     = realm.objects(Doctor.self).filter(doctorPredicate).first?.name ?? "The doctor is determining".localized()
        let specialityName = realm.objects(Speciality.self).filter(specialityPredicate).first?.name ?? "Speciality is determining".localized()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        
        cell.setSelectedBackgroundView()
        cell.textLabel?.numberOfLines = 0
        
        cell.textLabel?.font       = UIFont.systemFont(ofSize: 16.0)
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)

        cell.textLabel?.text       = clinicName + ". " + specialityName + ". " + doctorName + "."
        cell.detailTextLabel?.text = dateFormatter.string(from: appointment.dateTime)
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let appointment = self.appointments[indexPath.row]
        
        tableView.setEditing(false, animated: true)
        
        if editingStyle == .delete, let token = UserDataManager.sharedManager.currentAccessToken?.token  {
            
            let message = "Are you sure that you want to cancel the appointment to the doctor?".localized()
            let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Yes".localized(), style: .default) { (action) in
                
                AppointmentsDataManager.sharedManager.deleteAppointment(appointment: appointment, token: token, success: {
                    
                    self.deleteSupportObjectsFor(appointment: appointment)

                    self.removeNotificationAt(date: appointment.dateTime) //Remove pending notification from notification center.
                    
                    self.appointments.remove(at: indexPath.row)
                    
                    let predicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && doctorId == %@ && appointmentId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.doctorId, appointment.appointmentId])
                    
                    do {
                        try realm.write {
                            realm.delete(realm.objects(Appointment.self).filter(predicate))
                        }
                    } catch {
                        print("Can't delete appointment from Realm!!!")
                    }
                    
                    tableView.deleteRows(at: [indexPath], with: .left)
                    
                    self.checkAppointments()
                    
                    self.createAndPresentSimpleAlertWith(message: "Appointment is being canceled.".localized())
                    
                }, failure: { (error) in
                    
                    self.createAndPresentSimpleAlertWith(message: "Could not cancel appointment.".localized())
                    print(error ?? "Can't delete appointment!!!")
                })
            }
            
            let noAction = UIAlertAction(title: "No".localized(), style: .default, handler: nil)
            
            alertVC.addAction(noAction)
            alertVC.addAction(yesAction)
            
            self.present(alertVC, animated: true, completion: nil)
            
        } else {
            self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    //MARK: - MyMethods
    fileprivate func settingController() {
        self.settingView()
        self.getAppointments()
        self.createMenuBarBtn()
        self.addRefreshControl()
        self.createClinicsBarBtn()
    }
    
    func getAppointments() {
        
        self.createAndRunActivityIndicatorView()

        self.getAppointmentsFromServer(firstAppearance: true) {
            self.stopActivityIndicatorView()
        }
    }
    
    @objc func refreshData() {
        
        if #available(iOS 10.0, *) {

            self.getAppointmentsFromServer(firstAppearance: false) {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    fileprivate func getAppointmentsFromServer(firstAppearance: Bool, _ stopActInd: @escaping () -> ()) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                AppointmentsDataManager.sharedManager.getMyAppointments(token: token, success: { (newAppointments) in
                    
                    self.appointments = newAppointments.filter({ $0.dateTime > Date() }).sorted(by: { $0.dateTime < $1.dateTime })
                    
                    self.getSupportObjectsFor(appointments: self.appointments, token: token)
                    
                    stopActInd()
                    
                    self.firstAppearance = firstAppearance
                    
                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                    self.checkAppointments()
                    
                }, failure: { (error) in
                    
                    stopActInd()
                    
                    self.getAppointmentsFromRealm()
                    print(error ?? "Can't get clinics!!!")
                    
                })
                
            } else {
                
                stopActInd()
                self.getAppointmentsFromRealm()
                print("Haven't access token!!!")
            }
            
        } else {
            
            stopActInd()
            self.getAppointmentsFromRealm()
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func getAppointmentsFromRealm() {
        
        self.stopActivityIndicatorView()
        
        self.appointments = Array(realm.objects(Appointment.self)).filter({ $0.dateTime > Date() }).sorted(by: { $0.dateTime < $1.dateTime })
        
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
        
        self.checkAppointments()
    }

    
    fileprivate func getSupportObjectsFor(appointments: [Appointment], token: String) {
        
        for (index, appointment) in appointments.enumerated() {
            
            let clinicPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@", argumentArray: [appointment.hostId, appointment.clinicId])
            let doctorPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && doctorId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.doctorId])
            
            let specialityPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && specialityId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.specialityId])
            
            let indexPath = IndexPath(row: index, section: 0)
            
            if realm.objects(Clinic.self).filter(clinicPredicate).first == nil {
                
                AppointmentsDataManager.sharedManager.getClinicFor(appointment: appointment, token: token, success: { (clinic) in
                    
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    
                }, failure: { (error) in
                    
                    print(error ?? "Can't get clinic!!!")
                })
            }
            
            if realm.objects(Doctor.self).filter(doctorPredicate).first == nil {
                
                AppointmentsDataManager.sharedManager.getDoctorFor(appointment: appointment, token: token, success: { (doctor) in
                    
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    
                }, failure: { (error) in
                    
                    print(error ?? "Can't get doctor!!!")
                })
            }
            
            if realm.objects(Speciality.self).filter(specialityPredicate).first == nil {
                
                AppointmentsDataManager.sharedManager.getSpecialityFor(appointment: appointment, token: token, success: { (speciality) in
                    
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                    
                }, failure: { (error) in
                    
                    print(error ?? "Can't get speciality!!!")
                })
            }
        }
    }
    
    fileprivate func deleteSupportObjectsFor(appointment: Appointment) {
        
        let clinicPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@", argumentArray: [appointment.hostId, appointment.clinicId])
        let doctorPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && doctorId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.doctorId])
        
        let specialityPredicate = NSPredicate(format: "hostId == %@ && clinicId == %@ && specialityId == %@", argumentArray: [appointment.hostId, appointment.clinicId, appointment.specialityId])
        
        if let savedClinic = realm.objects(Clinic.self).filter(clinicPredicate).first {
            do {
                try realm.write {
                    realm.delete(savedClinic)
                }
            } catch {
                print("Can't delete clinic from Realm!!!")
            }
        }
        
        if let savedDoctor = realm.objects(Doctor.self).filter(doctorPredicate).first {
            do {
                try realm.write {
                    realm.delete(savedDoctor)
                }
            } catch {
                print("Can't delete doctor from Realm!!!")
            }
        }
        
        if let savedSpeciality = realm.objects(Speciality.self).filter(specialityPredicate).first {
            do {
                try realm.write {
                    realm.delete(savedSpeciality)
                }
            } catch {
                print("Can't delete speciality from Realm!!!")
            }
        }
    }
    
    fileprivate func createMenuBarBtn() {
        
        let image   = UIImage(named: "menu")
        let menuBtn = UIBarButtonItem(image: image, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        self.navigationItem.setLeftBarButton(menuBtn, animated: true)
    }
    
    fileprivate func createClinicsBarBtn() {
        let appointmentBtn = UIBarButtonItem(image: UIImage(named: "edit"), style: .done, target: self, action: #selector(goToClinicsVC))
        self.navigationItem.setRightBarButton(appointmentBtn, animated: true)
    }
    
    @objc func goToClinicsVC() {
        let clinicsVC = self.storyboard?.instantiateViewController(withIdentifier: self.clinicsVCId) as! ClinicsViewController
        self.navigationController?.pushViewController(clinicsVC, animated: true)
    }
    
    private func checkAppointments() {
        
        if self.appointments.isEmpty {
            
            self.noAppointmentsLabel.text = "No talons available.".localized()
            self.noAppointmentsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noAppointmentsLabel)
            
        } else {
            self.noAppointmentsLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "My appointments".localized()
        self.firstAppearance           = true
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
    
    fileprivate func removeNotificationAt(date: Date) {
        
        if #available(iOS 10.0, *) {
            
            let fullDateFormatter = DateFormatter()
            fullDateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
            
            let identifier = "appointment \(fullDateFormatter.string(from: date))"
            
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
        }
    }
}

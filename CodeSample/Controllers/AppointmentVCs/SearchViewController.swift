//
//  SearchViewController.swift
//  VistaProject
//
//  Created by Admin on 17.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noResultsLabel: UILabel!
    
    //MARK: - Properties
    var previousVC: UIViewController?
    var keyboardHeight: CGFloat?
    
    var doctors      = [Doctor]()
    var clinics      = [Clinic]()
    var specialities = [Speciality]()

    let datesVCId        = "DatesViewController"
    let doctorsVCId      = "DoctorsViewController"
    let specialitiesVCId = "SpecialitiesViewController"

    var sectionNames      = [String]()
    let clinicsTitle      = "Polyclinics".localized()
    let doctorsTitle      = "Doctors".localized()
    let specialitiesTitle = "Specialities".localized()

    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For automatic dimension height for table view row.
        self.tableView.rowHeight          = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        
        self.settingView()
        self.settingSearchBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscription()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscription()
    }
    
    //MARK: - UITableViewDataSource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionNames.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionNames[section]
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if sectionNames[section].hasPrefix(self.clinicsTitle) {
            
            return self.clinics.count
            
        } else if sectionNames[section].hasPrefix(self.specialitiesTitle) {
            
            return self.specialities.count
            
        } else if sectionNames[section].hasPrefix(self.doctorsTitle) {
            
            return self.doctors.count
            
        }  else {
            
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "SearchCell"
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        cell.textLabel?.numberOfLines = 0
        
        cell.textLabel?.font       = UIFont.systemFont(ofSize: 16.0)
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)
        
        if sectionNames[indexPath.section].hasPrefix(self.clinicsTitle) {
            
            let clinic = self.clinics[indexPath.row]
            
            cell.textLabel?.text       = clinic.name
            cell.detailTextLabel?.text = clinic.phone
            
        } else if sectionNames[indexPath.section].hasPrefix(self.specialitiesTitle) {
            
            let speciality =  self.specialities[indexPath.row]
            
            cell.textLabel?.text       = speciality.name
            cell.detailTextLabel?.text = "Free talons".localized() + ": " + "\(speciality.ticketCount)"
            
        } else if sectionNames[indexPath.section].hasPrefix(self.doctorsTitle) {
            
            let doctor =  self.doctors[indexPath.row]
            
            cell.textLabel?.text       = doctor.name
            cell.detailTextLabel?.text = "Free talons".localized() + ": " + "\(doctor.ticketCount)"
        }
        
        return cell
    }

    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.searchBar.resignFirstResponder()
        
        if self.sectionNames[indexPath.section].hasPrefix(self.clinicsTitle) {
            
            let specialitiesVC = self.storyboard?.instantiateViewController(withIdentifier: self.specialitiesVCId) as! SpecialitiesViewController
            specialitiesVC.clinic = self.clinics[indexPath.row]
            
            self.dismiss(animated: true, completion: {
                self.previousVC?.navigationController?.pushViewController(specialitiesVC, animated: true)
            })
            
        } else if self.sectionNames[indexPath.section].hasPrefix(self.specialitiesTitle) {
            
            let doctorsVC = self.storyboard?.instantiateViewController(withIdentifier: self.doctorsVCId) as! DoctorsViewController
            doctorsVC.speciality = self.specialities[indexPath.row]
            
            self.dismiss(animated: true, completion: { 
                self.previousVC?.navigationController?.pushViewController(doctorsVC, animated: true)
            })
            
        } else if self.sectionNames[indexPath.section].hasPrefix(self.doctorsTitle) {
            
            let datesVC = self.storyboard?.instantiateViewController(withIdentifier: self.datesVCId) as! DatesViewController
            datesVC.doctor = self.doctors[indexPath.row]
            
            self.dismiss(animated: true, completion: { 
                self.previousVC?.navigationController?.pushViewController(datesVC, animated: true)
            })
        }
    }
    
    //MARK: - UISearchBarDelegate
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.characters.count > 0, let token = UserDataManager.sharedManager.currentAccessToken?.token {
            
            AppointmentsDataManager.sharedManager.searchAt(text: searchText, token: token, success: { (clinics, specialities, doctors) in
                
                self.clinics      = clinics
                self.doctors      = doctors
                self.specialities = specialities
                
                self.sectionNames = [String]()
                
                if clinics.count != 0 {
                    self.sectionNames.append(self.clinicsTitle + " (\(self.clinics.count))")
                }
                
                if specialities.count != 0 {
                    self.sectionNames.append(self.specialitiesTitle + " (\(self.specialities.count))")
                }
                
                if doctors.count != 0 {
                    self.sectionNames.append(self.doctorsTitle + " (\(self.doctors.count))")
                }
                
                self.tableView.reloadData()
                self.checkResult()
                
            }, failure: { (error) in
                
                self.createAndPresentSimpleAlertWith(message: "Failed to search.".localized())
                self.checkResult()
                
                print(error ?? "Can't searh objects!!!")
                
            })
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    //MARK: - MyMethods
    fileprivate func subscription() {

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func unsubscription() {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        
        self.keyboardHeight = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect)?.height
        
        let frame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: self.keyboardHeight ?? 0)

        self.tableView.tableFooterView? = UIView(frame: frame)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        self.keyboardHeight = 0
        
        let frame = CGRect(x: 0, y: 0, width: self.tableView.bounds.width, height: self.keyboardHeight ?? 0)
        
        self.tableView.tableFooterView? = UIView(frame: frame)
    }
    
    fileprivate func checkResult() {
        
        if self.clinics.isEmpty && self.specialities.isEmpty && self.doctors.isEmpty {
            
            self.noResultsLabel.text     = "Nothing found.".localized()
            self.noResultsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noResultsLabel)
            
        } else {
            self.noResultsLabel.isHidden = true
        }
    }
    
    fileprivate func createAndPresentSimpleAlertWith(message: String) {
        
        let alertVC  = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized(), style: .default, handler: nil)
        
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    fileprivate func settingSearchBar() {
        
        self.searchBar.placeholder = "Search...".localized()
        
        if let textField = self.searchBar.value(forKey: "searchField") as? UITextField {
            if let label = textField.value(forKey: "placeholderLabel") as? UILabel {
                
                label.textColor = .black
            }
            
            if let iconView = textField.leftView as? UIImageView {
                
                iconView.image = iconView.image?.withRenderingMode(.alwaysTemplate)
                iconView.tintColor = .black
            }
        }
        
        for view in self.searchBar.subviews[0].subviews {
            if let btn = view as? UIButton {
                
                btn.setTitle("Cancel".localized(), for: .normal)
            }
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Search".localized()
        self.view.backgroundColor      = .white
    }
}

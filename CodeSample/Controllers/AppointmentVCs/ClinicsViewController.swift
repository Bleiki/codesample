//
//  ClinicsViewController.swift
//  VistaProject
//
//  Created by Admin on 10.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class ClinicsViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noClinicsLabel: UILabel!
    
    //MARK: - Properties
    let serachVCId                   = "SearchViewController"
    var sectionNames                 = [String]()
    var tracedSections               = [Int]()
    let specialitiesVCId             = "SpecialitiesViewController"
    var clinicsWithRelation          = [Clinic]()
    var clinicsWithoutRelation       = [Clinic]()
    let sectionOfClinicsWithRelation = 0
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For automatic dimension height for table view row.
        self.tableView.rowHeight          = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0

        self.getClinics()
        self.settingView()
        self.createSearchBarBtn()
    }
    
    //MARK: - UITableViewDataSource
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionNames.count
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sectionNames[section]
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !self.clinicsWithRelation.isEmpty {
            return section == sectionOfClinicsWithRelation ? self.clinicsWithRelation.count : self.clinicsWithoutRelation.count
        } else {
            return self.clinicsWithoutRelation.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ClinicsCell"
        let clinic     = self.clinicAt(indexPath: indexPath)
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text          = clinic.name
        cell.textLabel?.font          = UIFont.systemFont(ofSize: 16.0)
        cell.textLabel?.numberOfLines = 0

        cell.detailTextLabel?.text = clinic.phone
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)

        return cell
    }

    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == self.sectionOfClinicsWithRelation && !self.clinicsWithRelation.isEmpty {
            
            let specialitiesVC = self.storyboard?.instantiateViewController(withIdentifier: self.specialitiesVCId) as! SpecialitiesViewController
            specialitiesVC.clinic = self.clinicAt(indexPath: indexPath)
            
            self.navigationController?.pushViewController(specialitiesVC, animated: true)
            
        } else {
            
            let title   = "Continue?".localized()
            let message = "You are not affiliated with the selected health facility. You can only see the doctors schedule. For appointment, contact the registry.".localized()
            let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let yesAction = UIAlertAction(title: "Yes".localized(), style: .default, handler: { (action) in
                
                let specialitiesVC = self.storyboard?.instantiateViewController(withIdentifier: self.specialitiesVCId) as! SpecialitiesViewController
                specialitiesVC.clinic = self.clinicAt(indexPath: indexPath)
                
                self.navigationController?.pushViewController(specialitiesVC, animated: true)
            })
            
            let noAction = UIAlertAction(title: "No".localized(), style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            })
            
            alertVC.addAction(noAction)
            alertVC.addAction(yesAction)
            
            self.present(alertVC, animated: true, completion: nil)
        }
    }
    
    override public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !self.tracedCells.contains(indexPath) {
                
            // Add animation.
            let indexOfSection       = indexPath.section + 1
            let additionalRows       = indexPath.section * self.clinicsWithRelation.count
            let animationDuration    = 0.65
            let animationDelay       = firstAppearance! ? 0.05 * Double(indexPath.row + indexOfSection + additionalRows) : 0
            let XTranslation         = -tableView.bounds.width
            let translationTransform = CATransform3DMakeTranslation(XTranslation, 0, 0)
            let resultTransform      = CATransform3DScale(translationTransform, 0, 0, 1)
            
            cell.layer.transform = resultTransform
            
            UIView.animate(withDuration: animationDuration, delay: animationDelay, options: .curveEaseInOut, animations: {
                cell.layer.transform = CATransform3DIdentity
            })
            
            if let rows = self.visibleRowsAtFirstAppear, indexPath == rows.last {
                self.firstAppearance = false
            }
            
            self.tracedCells.append(indexPath)
        }
    }
    
    public func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        if !self.tracedSections.contains(section) {
            
            // Add animation.
            let animationDuration    = 0.65
            let animationDelay       = firstAppearance! ? 0.05 * Double(section + section * self.clinicsWithRelation.count) : 0
            let XTranslation         = -tableView.bounds.width
            let translationTransform = CATransform3DMakeTranslation(XTranslation, 0, 0)
            let resultTransform      = CATransform3DScale(translationTransform, 0, 0, 1)
            
            view.layer.transform = resultTransform
            
            UIView.animate(withDuration: animationDuration, delay: animationDelay, options: .curveEaseInOut, animations: {
                view.layer.transform = CATransform3DIdentity
            })
            
            self.tracedSections.append(section)
        }
    }

    //MARK: - MyMethods
    fileprivate func getClinics() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()
                
                AppointmentsDataManager.sharedManager.getMyNetrikaRelations(token: token, success: { (relations) in
                    
                    AppointmentsDataManager.sharedManager.getAllClinics(token: token, success: { (clinics) in
                        
                        self.group(clinics: clinics, by: relations)

                        self.stopActivityIndicatorView()
                        
                        self.tableView.reloadData()
                        self.setVisibleRowsFor(tableView: self.tableView)
                        
                        self.checkClinics()

                    }, failure: { (error) in
                        
                        self.stopActivityIndicatorView()
                        
                        self.createAndPresentSimpleAlertWith(message: "Could not get a list of available polyclinics.".localized())
                        print(error ?? "Can't get clinics!!!")
                        
                    })
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()
                    
                    self.createAndPresentSimpleAlertWith(message: "Could not get a list of attached polyclinics.".localized())
                    print(error ?? "Can't get my netrika relations!!!")
                    
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                print("Haven't access token!!!")
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func group(clinics: [Clinic], by relations:[Relation]) {
        
        if !relations.isEmpty {
            
            for relation in relations {
                
                let correctClinics = clinics.filter({ (clinic) -> Bool in
                    return clinic.hostId == relation.hostId && clinic.clinicId == relation.clinicId
                })
                
                self.clinicsWithRelation.append(contentsOf: correctClinics)
            }
            
            self.clinicsWithRelation = self.clinicsWithRelation.sorted(by: { $0.name < $1.name })
            
            let setOfAllClinics             = Set(clinics)
            let setOfClinicsWithoutRelation = setOfAllClinics.subtracting(self.clinicsWithRelation)
            
            self.clinicsWithoutRelation = Array(setOfClinicsWithoutRelation).sorted(by: { $0.name < $1.name })
            
            self.sectionNames = ["Attached".localized() + " (\(self.clinicsWithRelation.count))", "Unattached".localized() + " (\(self.clinicsWithoutRelation.count))"]
            
        } else {
            
            self.clinicsWithoutRelation = clinics
            self.sectionNames = ["Unattached".localized() + " (\(self.clinicsWithoutRelation.count))"] //For true animation.
        }
    }
    
    fileprivate func createSearchBarBtn() {
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(goToSearchVC))
        
        self.navigationItem.setRightBarButton(searchBtn, animated: true)
    }
    
    @objc func goToSearchVC() {
        
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: self.serachVCId) as! SearchViewController
        searchVC.previousVC = self
        
        self.present(searchVC, animated: true, completion: nil)
    }
    
    fileprivate func clinicAt(indexPath: IndexPath) -> Clinic {
        
        if !self.clinicsWithRelation.isEmpty {
            return indexPath.section == self.sectionOfClinicsWithRelation ? self.clinicsWithRelation[indexPath.row] : self.clinicsWithoutRelation[indexPath.row]
        } else {
            return self.clinicsWithoutRelation[indexPath.row]
        }
    }
    
    fileprivate func checkClinics() {
        
        if self.clinicsWithRelation.isEmpty && self.clinicsWithoutRelation.isEmpty {
            
            self.noClinicsLabel.text     = "There are no polyclinics available.".localized()
            self.noClinicsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noClinicsLabel)
            
        } else {
            self.noClinicsLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Polyclinics".localized()
        self.firstAppearance           = true
    }
}

//
//  DoctorsViewController.swift
//  VistaProject
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class DoctorsViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noDoctorsLabel: UILabel!
    
    //MARK: - Properties
    var doctors    = [Doctor]()
    let datesVCId  = "DatesViewController"
    let serachVCId = "SearchViewController"
    
    var clinic: Clinic!
    var speciality: Speciality!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getDoctors()
        self.settingView()
        self.createSearchBarBtn()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.doctors.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "DoctorsCell"
        let doctor     = self.doctors[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text       = doctor.name
        cell.detailTextLabel?.text = "Free talons".localized() + ": " + "\(doctor.ticketCount)"
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let datesVC = self.storyboard?.instantiateViewController(withIdentifier: self.datesVCId) as! DatesViewController
        
        datesVC.doctor = self.doctors[indexPath.row]
        datesVC.clinic = self.clinic
        
        self.navigationController?.pushViewController(datesVC, animated: true)
    }
    
    //MARK: - MyMethods
    fileprivate func getDoctors() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.createAndRunActivityIndicatorView()

                AppointmentsDataManager.sharedManager.getDoctorsFromSpecialty(speciality: self.speciality, token: token, success: { (doctors) in
                    
                    self.doctors = doctors.sorted(by: { $0.name < $1.name })
                    
                    self.stopActivityIndicatorView()

                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)

                    self.checkDoctors()
                    
                }, failure: { (error) in
                    
                    self.stopActivityIndicatorView()

                    self.createAndPresentSimpleAlertWith(message: "Could not get a list of doctors.".localized())
                    print(error ?? "Can't get doctors!!!")
                    
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Access problems.".localized())
                print("Haven't access token!!!")
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func createSearchBarBtn() {
        
        let searchBtn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(goToSearchVC))
        
        self.navigationItem.setRightBarButton(searchBtn, animated: true)
    }
    
    @objc func goToSearchVC() {
        
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: self.serachVCId) as! SearchViewController
        searchVC.previousVC = self
        
        self.present(searchVC, animated: true, completion: nil)
    }
    
    fileprivate func checkDoctors() {
        
        if self.doctors.isEmpty {
            
            self.noDoctorsLabel.text     = "There are no doctors available.".localized()
            self.noDoctorsLabel.isHidden = false
            self.view.bringSubview(toFront: self.noDoctorsLabel)
            
        } else {
            self.noDoctorsLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Doctors".localized()
        self.firstAppearance           = true
    }
}

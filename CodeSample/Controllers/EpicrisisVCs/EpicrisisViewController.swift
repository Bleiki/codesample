//
//  EpicrisisViewController.swift
//  VistaProject
//
//  Created by Admin on 13.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class EpicrisisViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noEpicrisisLabel: UILabel!
    
    //MARK: - ConstantsAndVariables
    var results              = [Result]()
    let htmlEpicrisisVCId    = "HTMLEpicrisisViewController"
    var lastSelectedIndexPath: IndexPath?

    //MARK: - ViewControllerMethods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingView()
        self.getEpicrisis()
        self.createMenuBarBtn()
        self.addRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateAccessoryType()
    }

    //MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.results.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let identifier = "EpicrisisCell"
        let epicrisis  = results[indexPath.row]
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        
        cell.setSelectedBackgroundView()
        
        cell.textLabel?.text       = epicrisis.name
        cell.detailTextLabel?.text = dateFormatter.string(from: epicrisis.created)
        
        if epicrisis.pathToHtmlFile != nil {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .disclosureIndicator
        }
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.lastSelectedIndexPath = indexPath //For update accessory type.
        
        let htmlEpicrisisVC = self.storyboard?.instantiateViewController(withIdentifier: htmlEpicrisisVCId) as! HTMLEpicrisisViewController
        
        htmlEpicrisisVC.epicrisis = self.results[indexPath.row]
        
        self.navigationController?.pushViewController(htmlEpicrisisVC, animated: true)
    }

    //MARK: - MyMethods
    func getEpicrisis() {
        
        self.createAndRunActivityIndicatorView()

        self.getEpicrisisFromServer(firstAppearance: true) {
            self.stopActivityIndicatorView()
        }
    }
    
    @objc func refreshData() {
        
        if #available(iOS 10.0, *) {
            
            self.getEpicrisisFromServer(firstAppearance: false) {
                
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    fileprivate func getEpicrisisFromServer(firstAppearance: Bool, _ stopActInd: @escaping () -> ()) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                EpicrisisDataManager.sharedManager.getEpicrisis(token: token, success: { (results, fragments) in
                    
                    self.results = results
                    
                    stopActInd()
                    
                    self.firstAppearance = firstAppearance
                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                    self.checkEpicrisis()
                    
                }, failure: { (error) in
                    
                    stopActInd()
                    
                    self.getEpicrisisFromRealm()
                    print(error ?? "Can't get epicrisis!!!")
                    
                })
                
            } else {
                
                stopActInd()
                self.getEpicrisisFromRealm()
                print("Haven't access token!!!")
            }
            
        } else {
            
            stopActInd()
            self.getEpicrisisFromRealm()
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func getEpicrisisFromRealm() {
        
        self.results = Array(realm.objects(Result.self))
        
        self.firstAppearance = true
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
        
        self.checkEpicrisis()
    }

    
    fileprivate func createMenuBarBtn() {
        
        let image   = UIImage(named: "menu")
        let menuBtn = UIBarButtonItem(image: image, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        self.navigationItem.setLeftBarButton(menuBtn, animated: true)
    }
    
    fileprivate func updateAccessoryType() {
        
        //For update daccessory type.
        if self.lastSelectedIndexPath != nil {
            
            let epicrisis = self.results[self.lastSelectedIndexPath!.row]
            
            if epicrisis.pathToHtmlFile != nil {
                self.tableView.reloadRows(at: [self.lastSelectedIndexPath!], with: .none)
            }
        }
    }
    
    fileprivate func checkEpicrisis() {
    
        if self.results.isEmpty {
            
            self.noEpicrisisLabel.text = "No epicrisis available.".localized()
            self.noEpicrisisLabel.isHidden = false
            self.view.bringSubview(toFront: self.noEpicrisisLabel)
            
        } else {
            self.noEpicrisisLabel.isHidden = true
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Research".localized()
        self.firstAppearance           = true
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
}

//
//  HTMLEpicrisisViewController.swift
//  VistaProject
//
//  Created by Admin on 14.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit
import Foundation

class HTMLEpicrisisViewController: BaseViewController, UIWebViewDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var webView: UIWebView!
    
    //MARK: - Properties
    var htmlString: String?
    var epicrisis: Result!
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.selectionOfFollowUpActions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        Orientation.lockOrientation(orientation: .allButUpsideDown)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        Orientation.lockOrientation(orientation: .portrait, andRotateTo: .portrait)
    }

    //MARK: - UIWebViewDelegate
    public func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopActivityIndicatorView()
    }

    //MARK: - MyMethods
    fileprivate func selectionOfFollowUpActions() {
        
        self.createAndRunActivityIndicatorView()
        
        self.navigationItem.title = epicrisis.name
        
        if self.epicrisis.pathToHtmlFile != nil {
            
            self.createShareBarBtn()
            self.showHTMLFile()
            
        } else {
            
            self.createSaveBarBtn()
            self.loadHTMLFile()
        }
    }
    
    fileprivate func showHTMLFile() {
        
        do {
            let path = self.epicrisis.pathToHtmlFile
            let url  = URL(fileURLWithPath: path!)
            
            self.htmlString = try String(contentsOf: url, encoding: .utf8)
            
            self.webView.loadHTMLString(self.htmlString ?? "", baseURL: nil)
            
        } catch {
            self.createAndPresentSimpleAlertWith(message: "Could not display epicrisis.".localized())
            print("Can't print html string!!!")
        }
    }
    
    fileprivate func loadHTMLFile() {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                
                EpicrisisDataManager.sharedManager.getHTMLFile(token: token, id: self.epicrisis.id, success: { (htmlString) in
                    
                    self.htmlString = htmlString
                    self.webView.loadHTMLString(htmlString, baseURL: nil)
                    
                    self.navigationItem.rightBarButtonItem?.isEnabled = true
                    
                }, failure: { (error) in
                    
                    self.createAndPresentSimpleAlertWith(message: "Could not download epicrisis.".localized())
                    
                    self.stopActivityIndicatorView()
                    self.navigationItem.rightBarButtonItem?.isEnabled = false
                    
                    print(error ?? "Can't get HTML string!!!")
                    
                })
                
            } else {
                
                self.createAndPresentSimpleAlertWith(message: "Could not download epicrisis.".localized())
                self.stopActivityIndicatorView()
                self.navigationItem.rightBarButtonItem?.isEnabled = false
            }
            
        } else {
            
            self.createAndPresentSimpleAlertWith(message: "No internet conection.".localized())
            self.stopActivityIndicatorView()
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    fileprivate func createSaveBarBtn() {
        let saveBtn = UIBarButtonItem(image: UIImage(named: "download"), style: .done, target: self, action: #selector(saveHtmlFile))
        self.navigationItem.setRightBarButton(saveBtn, animated: true)
    }
    
    @objc func saveHtmlFile() {
        
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
        let fileName = self.getFileNameFor(format: ".html")
        
        if let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            
            let urlStr = (path as NSString).appendingPathComponent(fileName)
            
            do {
                try self.htmlString?.write(toFile: urlStr, atomically: true, encoding: .utf8)
                
                try realm.write {
                    self.epicrisis.pathToHtmlFile = urlStr
                }
                
                self.createAndPresentSimpleAlertWith(message: "Epicrisis saved.".localized())
                
                self.createShareBarBtn()

            } catch {
                
                self.createAndPresentSimpleAlertWith(message: "Could not save epicrisis.".localized())
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("Can't save html file!!!")
            }
            
        } else {
            
            self.createAndPresentSimpleAlertWith(message: "Could not save epicrisis.".localized())
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            print("Can't save html file!!!")
        }
    }
    
    fileprivate func createShareBarBtn() {
        let shareBtn = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareHtmlFile))
        self.navigationItem.setRightBarButton(shareBtn, animated: true)
    }
    
    @objc func shareHtmlFile() {
        
        let pathToPdfFile = self.getPdfFrom(htmlString: self.htmlString ?? "")
        let url = URL(fileURLWithPath: pathToPdfFile)
        
        let activityVC = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func getPdfFrom(htmlString: String) -> String {
        
        let formatter = UIMarkupTextPrintFormatter(markupText: self.htmlString ?? "")
        
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(formatter, startingAtPageAt: 0)
        
        let page = CGRect(x: 0, y: 20, width: 595.2, height: 801.8) //A4, 72dpi
        
        render.setValue(NSValue.init(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue.init(cgRect: page), forKey: "printableRect")
        
        let pdfData = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(pdfData, CGRect.zero, nil)
        
        for i in 1...render.numberOfPages {
            
            UIGraphicsBeginPDFPage()
            
            let bounds = UIGraphicsGetPDFContextBounds()
            render.drawPage(at: i - 1, in: bounds)
        }
        
        UIGraphicsEndPDFContext()
        
        let fileName = self.getFileNameFor(format: ".pdf")
        let path     = "\(NSTemporaryDirectory())" + fileName
        
        pdfData.write(toFile: path, atomically: true)
        
        return path
    }
    
    func getFileNameFor(format: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        
        let id   = self.epicrisis.id
        let name = self.epicrisis.name.replacingOccurrences(of: " ", with: "_")
        let date = dateFormatter.string(from: self.epicrisis.created)
        
        return "\(date)" + "_" + "\(name)" + "_" + "\(id)" + format
    }
}

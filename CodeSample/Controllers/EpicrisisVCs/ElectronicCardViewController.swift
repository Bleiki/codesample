//
//  ElectronicCardViewController.swift
//  VistaProject
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class ElectronicCardViewController: BaseViewController, UITableViewDataSource {

    //MARK: - IBOutlets
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noEpicrisisLabel: UILabel!
    
    //MARK: - Properties
    var fragments            = [Fragment]()
    var diagnoses            = [Diagnosis]()
    let diagnosesIndex       = 0
    let fragmentsIndex       = 1
    let htmlEpicrisisVCId    = "HTMLEpicrisisViewController"
    var lastSelectedIndexPath: IndexPath?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //For automatic dimension height for table view row.
        self.tableView.rowHeight          = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44.0
        
        self.settingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateAccessoryType()
    }

    //MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.segmentedControl.selectedSegmentIndex == self.diagnosesIndex ? self.diagnoses.count : self.fragments.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "ElectronicCardCell"
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        
        cell.setSelectedBackgroundView()
        cell.textLabel?.numberOfLines = 0
        
        if self.segmentedControl.selectedSegmentIndex == self.diagnosesIndex {
            
            let diagnosis = self.diagnoses[indexPath.row]
            
            cell.accessoryType         = .disclosureIndicator
            cell.textLabel?.text       = diagnosis.name ?? "No diagnosis".localized()
            cell.detailTextLabel?.text = diagnosis.mkb ?? "No code".localized()

        } else {
            
            let fragment = self.fragments[indexPath.row]
            
            cell.textLabel?.text       = fragment.name
            cell.detailTextLabel?.text = dateFormatter.string(from: fragment.created)
            
            if fragment.pathToHtmlFile != nil {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .disclosureIndicator
            }
        }
        
        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if self.segmentedControl.selectedSegmentIndex == self.diagnosesIndex {
            
            self.segmentedControl.selectedSegmentIndex = self.fragmentsIndex
            
            let diagnosis = self.diagnoses[indexPath.row]
            
            self.fragments = self.fragments.filter({ $0.diagName == diagnosis.name && $0.mkb == diagnosis.mkb })
            
            self.tracedCells.removeAll()
            
            self.firstAppearance = true
            self.tableView.reloadData()
            self.setVisibleRowsFor(tableView: self.tableView)
            
            self.checkEpicrisis()
            
        } else {
            
            self.lastSelectedIndexPath = indexPath //For update accessory type.
            
            let htmlEpicrisisVC = self.storyboard?.instantiateViewController(withIdentifier: htmlEpicrisisVCId) as! HTMLEpicrisisViewController
            
            htmlEpicrisisVC.epicrisis = self.fragments[indexPath.row]
            
            self.navigationController?.pushViewController(htmlEpicrisisVC, animated: true)
        }
    }

    //MARK: - IBActions
    @IBAction func selecteResultsOrFragmentsAction(_ sender: UISegmentedControl) {
        
        self.tracedCells.removeAll()
        self.firstAppearance = true
        
        self.getDiagnosesAndFragmentsFromRealm()
    }
    
    //MARK: - MyMethods
    func getDiagnosesAndFragments() {
        
        self.createAndRunActivityIndicatorView()

        self.getDiagnosesAndFragmentsFromServer(firstAppearance: true) {
            self.stopActivityIndicatorView()
        }
    }
    
    @objc func refreshData() {
        
        if #available(iOS 10.0, *) {

            self.getDiagnosesAndFragmentsFromServer(firstAppearance: false) {
                
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
    
    fileprivate func getDiagnosesAndFragmentsFromServer(firstAppearance: Bool, _ stopActInd: @escaping () -> ()) {
        
        if Connection.isConnectedToNetwork() {
            if let token = UserDataManager.sharedManager.currentAccessToken?.token {
                
                EpicrisisDataManager.sharedManager.getEpicrisis(token: token, success: { (results, fragments) in
                    
                    self.fragments = fragments
                    self.getDiagnosesFromFragments(fragments: self.fragments)
                    
                    stopActInd()
                    
                    self.firstAppearance = firstAppearance
                    self.tableView.reloadData()
                    self.setVisibleRowsFor(tableView: self.tableView)
                    
                    self.checkEpicrisis()
                    
                }, failure: { (error) in
                    
                    stopActInd()
                    
                    self.getDiagnosesAndFragmentsFromRealm()
                    print(error ?? "Can't get diagnoses and fragments!!!")
                    
                })
                
            } else {
                
                stopActInd()
                self.getDiagnosesAndFragmentsFromRealm()
                print("Haven't access token!!!")
            }
            
        } else {
            
            stopActInd()
            self.getDiagnosesAndFragmentsFromRealm()
            print("Haven't network connection!!!")
        }
    }
    
    fileprivate func getDiagnosesAndFragmentsFromRealm() {

        self.fragments = Array(realm.objects(Fragment.self))
        
        self.getDiagnosesFromFragments(fragments: self.fragments)
        
        self.tableView.reloadData()
        self.setVisibleRowsFor(tableView: self.tableView)
        
        self.checkEpicrisis()
    }
    
    fileprivate func getDiagnosesFromFragments(fragments: [Fragment]) {
        
        for fragment in self.fragments {
            
            let diagnosis = Diagnosis(name: fragment.diagName, mkb: fragment.mkb)
            
            if !self.diagnoses.contains(diagnosis) {
                self.diagnoses.append(diagnosis)
            }
        }
        
        self.diagnoses = self.diagnoses.sorted { (first, second) -> Bool in
            
            if let firstName = first.name, let secondName = second.name {
                
                return firstName < secondName
                
            } else if first.name == nil && second.name == nil {
                
                return true
                
            } else if first.name != nil && second.name == nil  {
                
                return true
                
            } else if first.name == nil && second.name != nil {
                
                return false
                
            } else {
                
                return false
            }
        }        
    }
    
    fileprivate func settingSegmentedControl() {
        
        self.segmentedControl.setTitle("Diagnoses".localized(), forSegmentAt: 0)
        self.segmentedControl.setTitle("Documents".localized(), forSegmentAt: 1)
        
        self.segmentedControl.backgroundColor = .clear
        self.segmentedControl.tintColor       = .black
    }
    
    fileprivate func createMenuBarBtn() {
        
        let image   = UIImage(named: "menu")
        let menuBtn = UIBarButtonItem(image: image, style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        self.navigationItem.setLeftBarButton(menuBtn, animated: true)
    }
    
    fileprivate func updateAccessoryType() {
        
        //For update daccessory type.
        if self.lastSelectedIndexPath != nil {
            
            let fragment = self.fragments[self.lastSelectedIndexPath!.row]
            
            if fragment.pathToHtmlFile != nil {
                self.tableView.reloadRows(at: [self.lastSelectedIndexPath!], with: .none)
            }
        }
    }
    
    fileprivate func checkEpicrisis() {
        
        if self.segmentedControl.selectedSegmentIndex == self.diagnosesIndex {
            if self.diagnoses.isEmpty {
                
                self.noEpicrisisLabel.text = "No diagnoses available.".localized()
                self.noEpicrisisLabel.isHidden = false
                self.view.bringSubview(toFront: self.noEpicrisisLabel)
                
            } else {
                self.noEpicrisisLabel.isHidden = true
            }
            
        } else {
            
            if self.fragments.isEmpty {
                
                self.noEpicrisisLabel.text = "No documents available.".localized()
                self.noEpicrisisLabel.isHidden = false
                self.view.bringSubview(toFront: self.noEpicrisisLabel)
                
            } else {
                self.noEpicrisisLabel.isHidden = true
            }
        }
    }
    
    fileprivate func settingView() {
        
        self.tableView.tableFooterView = UIView()
        self.navigationItem.title      = "Electronic card".localized()
        self.firstAppearance           = true
        
        self.createMenuBarBtn()
        self.addRefreshControl()
        self.settingSegmentedControl()
        self.getDiagnosesAndFragments()
    }
    
    fileprivate func addRefreshControl() {

        if #available(iOS 10.0, *) {
            
            let refresh = UIRefreshControl()
            refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
            
            self.tableView.refreshControl  = refresh
        }
    }
}

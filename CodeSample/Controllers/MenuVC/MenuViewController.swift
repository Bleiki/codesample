//
//  MenuViewController.swift
//  VistaProject
//
//  Created by Admin on 20.06.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: - Properties
    let phoneIndex = 4
    let emailIndex = 5
    
    let identifiers = ["Профиль", "Электронная карта", "Исследования", "Запись на прием", "8 (812) 416-60-50", "tex.vistamed@gmail.com"]
    let images      = ["man", "medical-result", "copy", "speech-bubble", "phone", "mail"]
    let cells       = ["Profile".localized(), "Electronic card".localized(), "Research".localized(), "Appointment".localized(), "8 (812) 416-60-50", "tex.vistamed@gmail.com"]
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setGradientLayerAndImage()
    }
    
    //MARK: - TableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cells.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let identifier = identifiers[indexPath.row]
        let image      = UIImage(named: self.images[indexPath.row])
        let cell       = self.tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)

        cell.setSelectedBackgroundView()
        
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        
        cell.textLabel?.text  = cells[indexPath.row]
        cell.imageView?.image = image

        return cell
    }
    
    //MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case self.phoneIndex:
            
            if #available(iOS 10.0, *) {

                let url = URL(string: "tel://88124166050")
                
                if url != nil, UIApplication.shared.canOpenURL(url!) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                    
                } else {
                    self.createAndPresentSimpleAlertWith(message: "No possibility to call.".localized())
                }
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "No possibility to call.".localized())
            }
            
        case self.emailIndex:
            
            if !MFMailComposeViewController.canSendMail() {
                self.createAndPresentSimpleAlertWith(message: "No possibility to send E-mail.".localized())
            } else {
                self.sendEmail()
            }

        default:
            break
        }
    }
    
    //MARK: - MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .cancelled:
            
            controller.dismiss(animated: true, completion: nil)
            
        case .failed:
            
            controller.dismiss(animated: true, completion: nil)
            self.createAndPresentSimpleAlertWith(message: "Failed to send E-mail.".localized())
            
        case .saved:
            
            controller.dismiss(animated: true, completion: nil)
            self.createAndPresentSimpleAlertWith(message: "Draft saved.".localized())
            
        case .sent:
            
            controller.dismiss(animated: true, completion: nil)
            self.createAndPresentSimpleAlertWith(message: "E-mail was sent.".localized())
        }
    }
    
    //MARK: - MyMethods
    fileprivate func sendEmail() {
        
        let composeVC = MFMailComposeViewController()
        
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients(["tex.vistamed@gmail.com"])
        
        self.present(composeVC, animated: true, completion: nil)
    }
    
    fileprivate func setGradientLayerAndImage() {
        
        self.tableView.tableFooterView = UIView()
        
        let gradientLayer = CAGradientLayer()
        let rightColor    = UIColor(red: 13 / 255, green: 71 / 255, blue: 161 / 255, alpha: 1).cgColor
        let leftColor     = UIColor(red: 136 / 255, green: 201 / 255, blue: 246 / 255, alpha: 1).cgColor
        
        gradientLayer.colors = [leftColor, rightColor]
        gradientLayer.frame  = self.view.bounds
        gradientLayer.startPoint = CGPoint(x: 1, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        self.imageView.image = UIImage(named: "vista-logo")
    }
    
    fileprivate func createAndPresentSimpleAlertWith(message: String) {
        
        let alertVC  = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized(), style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
}

//
//  AutorizationViewController.swift
//  VistaProject
//
//  Created by Admin on 13.07.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit
import UserNotifications

class AutorizationViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    //MARK: - Properties
    let menuVCId         = "MenuViewController"
    let registrationVCId = "RegistrationViewController"
    
    var previousVC: UIViewController?
    var actIndView: UIActivityIndicatorView?
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    //MARK: - ViewControllerMethods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addGestureRecognizer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeGestureRecognizer()
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.loginTextField {
            
            self.passwordTextField.becomeFirstResponder()
            
        } else if textField == self.passwordTextField {
            
            self.passwordTextField.resignFirstResponder()
        }
        
        return true
    }
    
    //MARK: - IBActions
    @IBAction func logInAction(_ sender: UIButton) {
        
        self.removeFirstResponder()
        self.logIn()
    }

    @IBAction func registrationAction(_ sender: UIButton) {
        
        let registrationVC = self.storyboard?.instantiateViewController(withIdentifier: self.registrationVCId) as! RegistrationViewController
        
        self.present(registrationVC, animated: true, completion: nil)
    }
    
    //MARK: - MyMethods  
    fileprivate func logIn() {
        
        if Connection.isConnectedToNetwork() {
            
            if let login = self.loginTextField.text, let password = self.passwordTextField.text, login.characters.count >= 1, password.characters.count >= 1 {
                
                self.createAndRunActivityIndicatorView()
                
                UserDataManager.sharedManager.getAccessToken(login: login, password: password, success: {
                    
                    self.stopActivityIndicatorView()
                    
                    //Request for permission of local notifications.
                    if #available(iOS 10.0, *) {
                        
                        let center = UNUserNotificationCenter.current()
                        
                        center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                            
                            if error != nil {
                                print(error!.localizedDescription)
                            }
                        }
                    }
                    
                    //For correct dismiss.
                    if let profileVC = self.previousVC as? ProfileViewController {
                        
                        self.dismiss(animated: true, completion: { profileVC.getPatientInfo() })
                        
                    } else if self.previousVC != nil {
                        
                        let profileVC = self.previousVC?.navigationController?.viewControllers.first as? ProfileViewController
                        
                        self.previousVC?.navigationController?.popToRootViewController(animated: false)
                        self.dismiss(animated: true, completion: {
                            
                            if profileVC != nil {
                                profileVC!.getPatientInfo()
                            }
                        })
                        
                    } else {
                        
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }, failure: { (error) in
                    
                    self.createAndPresentSimpleAlertWith(message: "Could not log in.".localized())
                    self.stopActivityIndicatorView()
                    
                    print(error ?? "Can't get access token!!!")
                })
                
            } else {
                self.createAndPresentSimpleAlertWith(message: "Field login and password should not be empty!".localized())
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet conection.".localized())
        }
    }
    
    //For dismiss keyboard by tap.
    fileprivate func addGestureRecognizer() {
        
        if self.keyboardDismissTapGesture == nil {
            
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }
    
    fileprivate func removeGestureRecognizer() {
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    fileprivate func settingViews() {
        
        self.view.backgroundColor = .white
        
        self.imageView.image = UIImage(named: "vista-logo")

        self.loginLabel.text    = "Login".localized()
        self.passwordLabel.text = "Password".localized()
        
        self.loginButton.setTitle("Log in".localized(), for: .normal)
        self.registrationButton.setTitle("Registration".localized(), for: .normal)
        
        self.loginTextField.placeholder    = "Enter login...".localized()
        self.passwordTextField.placeholder = "Enter password...".localized()
    }
    
    fileprivate func removeFirstResponder() {
        
        if self.loginTextField.isFirstResponder {
            
            self.loginTextField.resignFirstResponder()
            
        } else if self.passwordTextField.isFirstResponder {
            
            self.passwordTextField.resignFirstResponder()
        }
    }
    
    fileprivate func createAndPresentSimpleAlertWith(message: String) {
        
        let alertVC  = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized(), style: .default, handler: nil)
        
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    fileprivate func createAndRunActivityIndicatorView() {
        
        let actIndView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        actIndView.center = self.view.center
        actIndView.startAnimating()
        
        self.view.addSubview(actIndView)
        self.view.bringSubview(toFront: actIndView)
        
        self.actIndView = actIndView
    }
    
    fileprivate func stopActivityIndicatorView() {
        self.view.sendSubview(toBack: self.actIndView!)
        self.actIndView?.stopAnimating()
    }
}

//
//  RegistrationViewController.swift
//  VistaMed
//
//  Created by Admin on 10.08.17.
//  Copyright © 2017 Vista. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate {

    //MARK: - IBOutlets   
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var patrNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var birthDateTextField: UITextField!
    @IBOutlet weak var repeatPasswordTextField: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    
    //MARK: - Properties
    var haveOffset  = false
    let popoverVCId = "PopoverViewController"
    var keyboardDismissTapGesture: UITapGestureRecognizer?
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.settingViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addGestureRecognizer()
        self.subscription()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeGestureRecognizer()
        self.unsubscription()
    }
    
    //MARK: - IBActions
    @IBAction func registrationAction(_ sender: UIButton) {
        
        if Connection.isConnectedToNetwork() {
            
            let email       = emailTextField.text
            var phone       = phoneTextField.text
            let login       = loginTextField.text
            var gender      = genderTextField.text
            let lastName    = lastNameTextField.text
            var patrName    = patrNameTextField.text
            let password    = passwordTextField.text
            let firstName   = firstNameTextField.text
            let birthDate   = birthDateTextField.text
            let repeatPassw = repeatPasswordTextField.text
            
            let haveText = self.haveText(email: email, phone: phone, login: login, gender: gender, lastName: lastName, patrName: patrName, password: password, firstName: firstName, birthDate: birthDate, repeatPassw: repeatPassw)
            
            if haveText {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                let birthday      = dateFormatter.date(from: birthDate!)
                let checkPhone    = phone!.replacingOccurrences(of: " ", with: "")
                let checkPatrName = patrName!.replacingOccurrences(of: " ", with: "")
                
                phone    = checkPhone.characters.count >= 1 ? checkPhone : nil
                patrName = checkPatrName.characters.count >= 1 ? checkPatrName : nil
                
                if gender == "Male".localized() {
                    gender = "MALE"
                } else if gender == "Female".localized() {
                    gender = "FEMALE"
                } else {
                    gender = nil
                }
                
                if password == repeatPassw {
                    
                    UserDataManager.sharedManager.registration(
                        login: login!,
                        email: email!,
                        phone: phone,
                        gender: gender,
                        password: password!,
                        lastName: lastName!,
                        patrName: patrName,
                        birthDate: birthday,
                        firstName: firstName!, success: {
                            
                            self.createAndPresentSimpleAlertWith(message: "Registration is successful. A link to confirm your registration has been sent to you E-mail address.".localized())
                            self.dismiss(animated: true, completion: nil)
                            
                    }, failure: { (error, message) in
                        
                        var alertText = "Registration is not successful.".localized()

                        if message != nil {
                            alertText += " (\(message!))"
                        } else {
                            print(error ?? "Can't registration!!!")
                        }
                        
                        self.createAndPresentSimpleAlertWith(message: alertText)
                    })
                    
                } else {
                    self.createAndPresentSimpleAlertWith(message: "Passwords must match.".localized())
                }
            } else {
                self.createAndPresentSimpleAlertWith(message: "Data entered incorrectly.".localized())
            }
        } else {
            self.createAndPresentSimpleAlertWith(message: "No internet connection.".localized())
        }
    }
    
    @IBAction func cancelAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.shouldReturn(textField: textField)
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == self.birthDateTextField {
            
            self.removeFirstResponder()
            self.presentPopoverWith(state: .date, height: 200, from: textField)
            
            return false
            
        } else if textField == self.genderTextField {
            
            self.removeFirstResponder()
            self.presentPopoverWith(state: .gender, height: 50, from: textField)
            
            return false
        }
        
        return true
    }
    
    //MARK: - UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        
        if let presentedVC = popoverPresentationController.presentedViewController as? PopoverViewController {
            if presentedVC.state == .date {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                let dateString = dateFormatter.string(from: presentedVC.datePicker.date)
                
                self.birthDateTextField.text = dateString
                
            } else if presentedVC.state == .gender {
                
                let index = presentedVC.segmentedControl.selectedSegmentIndex
                
                if index == 0 || index == 1 {
                    
                    let gender = presentedVC.segmentedControl.titleForSegment(at: index)
                    
                    self.genderTextField.text = gender
                }
            }
        }
    }
    
    //MARK: - MyMethods
    fileprivate func settingViews() {
        
        self.textView.text = "Attention! By pressing the button \"Registration\" you confirm your personal data transfer and processing (Last Name, First Name, Patronymic, gender, birth date) in accordance with p.4 ar.9 Federal Law № 152-ФЗ.".localized()
        
        self.navigationItem.title = "Registration".localized()
        self.view.backgroundColor = .white
        
        self.emailTextField.placeholder          = "E-mail..."
        self.phoneTextField.placeholder          = "Phone(optional)...".localized()
        self.loginTextField.placeholder          = "Login...".localized()
        self.genderTextField.placeholder         = "Gender(optional)...".localized()
        self.lastNameTextField.placeholder       = "Last name...".localized()
        self.patrNameTextField.placeholder       = "Patronymic(optional)...".localized()
        self.passwordTextField.placeholder       = "Password...".localized()
        self.birthDateTextField.placeholder      = "Date of birth(optional)...".localized()
        self.firstNameTextField.placeholder      = "First name...".localized()
        self.repeatPasswordTextField.placeholder = "Repeat password...".localized()
        
        self.cancelButton.setTitle("Cancel".localized(), for: .normal)
        self.registrationButton.setTitle("Registration".localized(), for: .normal)
    }
    
    fileprivate func presentPopoverWith(state: PopoverViewController.State, height: CGFloat, from textField: UITextField) {
        
        let popoverVC = self.storyboard?.instantiateViewController(withIdentifier: popoverVCId) as! PopoverViewController
        popoverVC.state = state
        
        popoverVC.modalPresentationStyle = .popover
        popoverVC.preferredContentSize   = CGSize(width: self.view.bounds.width, height: height)
        
        popoverVC.popoverPresentationController?.permittedArrowDirections = .up
        popoverVC.popoverPresentationController?.sourceView               = textField
        popoverVC.popoverPresentationController?.sourceRect               = textField.bounds
        popoverVC.popoverPresentationController?.delegate                 = self
        
        self.present(popoverVC, animated: true, completion: nil)
    }
    
    fileprivate func haveText(email: String?, phone: String?, login: String?, gender: String?, lastName: String?, patrName: String?, password: String?, firstName: String?, birthDate: String?, repeatPassw: String?) -> Bool {
        
        if email != nil, phone != nil, login != nil, gender != nil, lastName != nil, patrName != nil, password != nil, firstName != nil, birthDate != nil, repeatPassw != nil {
            
            let checkEmail       = email!.replacingOccurrences(of: " ", with: "")
            let checkLogin       = login!.replacingOccurrences(of: " ", with: "")
            let checkLastName    = lastName!.replacingOccurrences(of: " ", with: "")
            let checkPassword    = password!.replacingOccurrences(of: " ", with: "")
            let checkFirstName   = firstName!.replacingOccurrences(of: " ", with: "")
            let checkRepeatPassw = repeatPassw!.replacingOccurrences(of: " ", with: "")
            
            if checkEmail.characters.count >= 1, checkLogin.characters.count >= 1, checkLastName.characters.count >= 1, checkPassword.characters.count >= 1, checkFirstName.characters.count >= 1, checkRepeatPassw.characters.count >= 1 {
                
                return true
                
            } else {
                
                return false
            }
            
        } else {
            
            return false
        }
    }
    
    //For keyboard.
    fileprivate func subscription() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func unsubscription() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect)?.height
        
        if !self.haveOffset {
            self.scrollView.contentSize.height += keyboardHeight ?? 0
            self.haveOffset = !self.haveOffset
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        let keyboardHeight = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect)?.height
        
        if self.haveOffset {
            self.scrollView.contentSize.height -= keyboardHeight ?? 0
            self.haveOffset = !self.haveOffset
        }
    }
    
    //For dismiss keyboard by tap.
    fileprivate func addGestureRecognizer() {
        
        if self.keyboardDismissTapGesture == nil {
            
            self.keyboardDismissTapGesture = UITapGestureRecognizer(target: self.view, action: #selector(self.view.endEditing(_:)))
            self.keyboardDismissTapGesture?.cancelsTouchesInView = false
            
            self.view.addGestureRecognizer(self.keyboardDismissTapGesture!)
        }
    }
    
    fileprivate func removeGestureRecognizer() {
        
        if self.keyboardDismissTapGesture != nil {
            self.view.removeGestureRecognizer(self.keyboardDismissTapGesture!)
            self.keyboardDismissTapGesture = nil
        }
    }
    
    //For text fields.
    fileprivate func shouldReturn(textField: UITextField) {
        
        if textField == self.lastNameTextField {
            
            self.firstNameTextField.becomeFirstResponder()
            
        } else if textField == self.firstNameTextField {
            
            self.patrNameTextField.becomeFirstResponder()
            
        } else if textField == self.patrNameTextField {
            
            textField.resignFirstResponder()
            self.birthDateTextField.becomeFirstResponder()
            
        } else if textField == self.emailTextField {
            
            self.phoneTextField.becomeFirstResponder()
            
        } else if textField == self.phoneTextField {
            
            self.loginTextField.becomeFirstResponder()
            
        } else if textField == self.loginTextField {
            
            self.passwordTextField.becomeFirstResponder()
            
        } else if textField == self.passwordTextField {
            
            self.repeatPasswordTextField.becomeFirstResponder()
            
        } else if textField == self.repeatPasswordTextField {
            
            textField.resignFirstResponder()
        }
    }
    
    fileprivate func removeFirstResponder() {
        
        if self.lastNameTextField.isFirstResponder {
            
            self.lastNameTextField.resignFirstResponder()
            
        } else if self.firstNameTextField.isFirstResponder {
            
            self.firstNameTextField.resignFirstResponder()
            
        } else if self.patrNameTextField.isFirstResponder {
            
            self.patrNameTextField.resignFirstResponder()
            
        } else if self.emailTextField.isFirstResponder {
            
            self.emailTextField.resignFirstResponder()
            
        } else if self.phoneTextField.isFirstResponder {
            
            self.phoneTextField.resignFirstResponder()
            
        } else if self.loginTextField.isFirstResponder {
            
            self.loginTextField.resignFirstResponder()
            
        } else if self.passwordTextField.isFirstResponder {
            
            self.passwordTextField.resignFirstResponder()
            
        } else if self.repeatPasswordTextField.isFirstResponder {
            
            self.repeatPasswordTextField.resignFirstResponder()
        }
    }
    
    //Alert.
    fileprivate func createAndPresentSimpleAlertWith(message: String) {
        
        let alertVC  = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok".localized(), style: .default, handler: nil)
        
        alertVC.addAction(okAction)
        
        self.present(alertVC, animated: true, completion: nil)
    }
}
